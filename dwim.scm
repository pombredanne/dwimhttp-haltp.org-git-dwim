#!/usr/bin/ol -r

;; dwim - do what i mean

;; todo
;;  - $HOME/.dwimrc
;;  - correct quoting and check encoding info passing
;;  - first word based rules or a special character? (google: madagascar, define: flux, spell: anemony)
;;  - builtin X clipboard handling? safe quotation in of it in scripts is nasty

(import 
   (owl args) 
   (owl sys)
   (prefix (owl sys) sys-))

(define (first-line)
   (lets
      ((input (lines stdin))
       (first input (uncons input #false)))
      (or first "")))

;; (pattern name ...)
(define search-engines
   '(("http://www.google.com/search?q=%s" "google" "g")
     ("http://www.wolframalpha.com/input/?i=%s" "wolframalpha" "wa")
     ("http://duckduckgo.com/?q=%s&kp=-1&kl=us-en" "duckduckgo" "ddg")
     ("http://duckduckgo.com/lite?q=%s" "duckduckgolite" "ddgl")
     ("http://dictionary.reference.com/dic?q=%s&s=t" "dictionary" "dict")))

(define default-engine "duckduckgolite")

(define (subprocess cmd . args)
   (let ((pid (sys-fork)))
      (cond
         ((not pid) #false)
         ((eq? pid #true) 
            (print "execing " cmd " with args " (cons cmd args))
            (exec cmd (cons cmd args)))
         (else
            (equal? (sys-wait pid) (cons 1 0))))))
      
(define (fetch-file url)
   (subprocess "/usr/bin/wget" url))

(define (show-url url prog . args)
   (print "Trying to download " url ".")
   (if (fetch-file url)
      (let ((filename (s/.*\/// url)))
         (print "Opening " filename " with " prog ", args " args)
         (apply subprocess 
            (cons prog (append args (list filename)))))
      (print "Failed to fetch file.")))

(define (show-image url)
   (show-url url "/usr/bin/xli" "-fill"))

(define (show-video url)
   (show-url url "/usr/bin/mplayer" "-quiet" "-loop" "0" "-zoom"))

(define (engine-pattern name)
   (let loop ((es search-engines))
      (cond
         ((null? es) #false)
         ((mem equal? (cdar es) name) (caar es))
         (else (loop (cdr es))))))

(define command-line-rules
   (cl-rules
      `((engine "-e" "--engine" has-arg comment "choose search engine" 
           default ,default-engine)
        (list "-l" "--list" comment "list known engines")
        (browser "-b" "--browser" has-arg comment "web browser executable to use"
            default "/usr/bin/lynx")
        (help "-h" "--help" comment "show this thing")
        (about "-a" "--about" comment "what is this thing?")
        (repeat "-r" "--return" comment "return to dwim after processing query")
        (dir "-d" "--dir" has-arg comment "directory for running commands" default "/tmp")
        (home "-h" "--home" has-arg comment "meaning of empty input" default "http://haltp.org/aoh/wgo")
        (prompt "-p" "--prompt" has-arg comment "prompt to use when asking for input"
            default "dwim: ")
        ;(verbose "-v" "--verbose" comment "show progress during generation")
        )))

;; very strict url encoder
;; str → (byte ...)
(define (encode->list str)
   (foldr 
      (λ (char tl)
         (cond
            ((eq? char #\space)
               (cons #\+ tl))
            ((m/[a-zA-Z0-9]/ (list char))
               (cons char tl))
            (else
               (cons #\%
                  (append
                     (cdr
                        (string->list
                           (number->string (+ char 256) 16)))
                     tl)))))
      null (string->bytes str)))

(define (fill-pattern pat input-list)
   (list->string
      (foldr
         (λ (char tl)
            (if (eq? char #\%)
               (case (if (null? tl) #false (car tl))
                  ((#\s)
                     (append input-list (cdr tl)))
                  (else
                     (cons char tl)))
               (cons char tl)))
         null
         (string->list pat)))) ;; allow the query to have UTF-8 elsewhere

(define (browser dict url)
   (let ((bin (getf dict 'browser)))
      (exec bin (list bin url))))

(define image-suffixes
   (list "jpg" "jpeg" "png"))

(define video-suffixes
   (list "gif" "avi" "mpg" "mpeg"))

(define (dwim-query dict str)
   (cond
      ((m/^[ \t\n]*http[^ ]+[ \t\n]*$/ str)
         ;; no whitespace pruning here atm
         (let ((suffix (s/.*\.// str)))
            (print "suffix " suffix)
            (cond
               ((mem string-ci=? image-suffixes suffix)
                  (show-image str))
               ((mem string-ci=? video-suffixes suffix)
                  (show-video str))
               (else
                  (browser dict str)))))
      ((m/^[a-z0-9]+(\.[a-z0-9]+)+$/ str)
         (browser dict str))
      ((equal? str "wgo")
         ;; explicit for now before configs are loaded
         (browser dict "http://haltp.org/aoh/wgo"))
      ((equal? str "notes")
         ;; explicit for now before configs are loaded
         (browser dict "http://haltp.org/aoh/notes.txt"))
      ((engine-pattern (getf dict 'engine)) => 
         (λ (pat)
            (browser dict
               (fill-pattern pat (encode->list str)))))
      (else
         (print-to stderr "unknown engine. known ones are: " (foldr append null (map cdr search-engines)))
         1)))
   
(define usage-text "Usage: dwim [args] [string] ...")

(define about-text "dwim: handle input")

(define (maybe-change-directory dict)
   (let ((path (getf dict 'dir)))
      (if (and path (not (sys-chdir path)))
         (begin
            (print "ERROR: cannot chdir to " path)
            (halt 1)))))

(define (start-dwim dict args)
   (maybe-change-directory dict)
   (cond
      ((getf dict 'help) 
         (and 
            (print usage-text)
            (print-rules command-line-rules)
            0))
      ((getf dict 'about)
         (and
            (print about-text)
            0))
      ((getf dict 'list)
         (and
            (fold (λ (ok? thing) (and ok? (print  thing))) #true 
               (cons "Engines: "
                  (map 
                     (λ (thing)
                        (foldr 
                           (λ (a b) (string-append a (string-append " " b)))
                           (string-append "-> " (car thing))
                           (cons "  " (cdr thing))))
                     search-engines)))
            0))
      ((null? args)
         ;; no arguments given, read one line from stdin
         (display-to stderr 
            (get dict 'prompt "? "))
         (lets 
            ((first rest (uncons (lines stdin) ""))
             (first (if (equal? first "") (get dict 'home "") first)))
            (dwim-query dict first)))
      (else
         (dwim-query dict
            (foldr (λ (string tail) (if (equal? tail "") string (string-append string (string-append " " tail))))
               ""
               args)))))

(define (dwim-trampoline dict args)
   (if (getf dict 'repeat)
      (lets
         ((pid (sys-fork)))
         (cond
            ((not pid)
               (print "dwim: failed to fork child process.")
               1)
            ((eq? pid #true)
               (halt (start-dwim dict args)))
            (else
               (let ((res (sys-wait pid)))
                  (if (not (equal? res (cons 1 0))) ;; program didn't exit(0)
                     (print "Subprocess exited abnormally: " res))
                  (dwim-trampoline dict null)))))
      (start-dwim dict args)))
   

(define (dwim args)
   (process-arguments (cdr args) 
      command-line-rules usage-text dwim-trampoline))

dwim
