CC=cc
CFLAGS=-O1
OFLAGS=
PREFIX=/usr

everything: dwim .ok

.ok: dwim
	./test.sh ./dwim
	touch .ok

dwim: dwim.c
	$(CC) $(CFLAGS) -o dwim dwim.c

#dwim: dwim.fasl
#	echo '#!/usr/bin/owl-vm' > dwim
#	cat dwim.fasl >> dwim
#	chmod +x dwim

dwim.fasl: dwim.scm
	test -x owl-lisp/bin/ol || make owl-lisp/bin/ol
	owl-lisp/bin/ol -o dwim.fasl dwim.scm

dwim.c: dwim.scm
	test -x owl-lisp/bin/ol || make owl-lisp/bin/ol
	owl-lisp/bin/ol $(OFLAGS) -o dwim.c dwim.scm

install: dwim
	mkdir -p $(PREFIX)/bin
	install -m 755 dwim $(PREFIX)/bin/dwim

owl-lisp/bin/ol:
	# this may take a moment depending on your machine
	-git clone http://haltp.org/git/owl-lisp.git
	-cd owl-lisp && git pull
	cd owl-lisp && make


clean:
	-rm dwim dwim.c

uninstall:
	-rm $(PREFIX)/bin/dwim
