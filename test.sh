#!/bin/sh

fail() {
   echo "FAIL: $@"
   exit 1
}

WURL="$@ --browser /bin/echo"
$WURL slartibartfast | grep -q slartibartfast || fail "no ASCII input seen in output"

A=$($WURL foo bar)
B=$($WURL "foo bar")
C=$(echo foo bar | $WURL 2>/dev/null)

test "$A" = "$B" || fail "outputs differ 1: $A $B"
test "$A" = "$C" || fail "outputs differ 2: $A $C"
test "$B" = "$C" || fail "your computer is drunk"

for URL in "http://foo.tla" "https://foo.tla   "
do
   $WURL "$URL" | grep -q "$URL" || fail "url not passed through: $URL"
done

echo "$@ passed sanity tests"
